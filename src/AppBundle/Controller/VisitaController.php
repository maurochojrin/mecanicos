<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Recordatorio;
use AppBundle\Entity\Vehiculo;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\View\TwitterBootstrap3View;

use AppBundle\Entity\Visita;

/**
 * Visita controller.
 *
 * @Route("/visita")
 */
class VisitaController extends Controller
{
    /**
     * Lists all Visita entities.
     *
     * @Route("/", name="visita")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository('AppBundle:Visita')->createQueryBuilder('e');

        list($filterForm, $queryBuilder) = $this->filter($queryBuilder, $request);
        list($visitas, $pagerHtml) = $this->paginator($queryBuilder, $request);
        
        $totalOfRecordsString = $this->getTotalOfRecordsString($queryBuilder, $request);

        return $this->render('visita/index.html.twig', array(
            'visitas' => $visitas,
            'pagerHtml' => $pagerHtml,
            'filterForm' => $filterForm->createView(),
            'totalOfRecordsString' => $totalOfRecordsString,

        ));
    }

    /**
    * Create filter form and process filter request.
    *
    */
    protected function filter($queryBuilder, Request $request)
    {
        $session = $request->getSession();
        $filterForm = $this->createForm('AppBundle\Form\VisitaFilterType');

        // Reset filter
        if ($request->get('filter_action') == 'reset') {
            $session->remove('VisitaControllerFilter');
        }

        // Filter action
        if ($request->get('filter_action') == 'filter') {
            // Bind values from the request
            $filterForm->handleRequest($request);

            if ($filterForm->isValid()) {
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set('VisitaControllerFilter', $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has('VisitaControllerFilter')) {
                $filterData = $session->get('VisitaControllerFilter');
                
                foreach ($filterData as $key => $filter) { //fix for entityFilterType that is loaded from session
                    if (is_object($filter)) {
                        $filterData[$key] = $queryBuilder->getEntityManager()->merge($filter);
                    }
                }
                
                $filterForm = $this->createForm('AppBundle\Form\VisitaFilterType', $filterData);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        return array($filterForm, $queryBuilder);
    }


    /**
    * Get results from paginator and get paginator view.
    *
    */
    protected function paginator($queryBuilder, Request $request)
    {
        //sorting
        $sortCol = $queryBuilder->getRootAlias().'.'.$request->get('pcg_sort_col', 'id');
        $queryBuilder->orderBy($sortCol, $request->get('pcg_sort_order', 'desc'));
        // Paginator
        $adapter = new DoctrineORMAdapter($queryBuilder);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage($request->get('pcg_show' , 10));

        try {
            $pagerfanta->setCurrentPage($request->get('pcg_page', 1));
        } catch (\Pagerfanta\Exception\OutOfRangeCurrentPageException $ex) {
            $pagerfanta->setCurrentPage(1);
        }
        
        $entities = $pagerfanta->getCurrentPageResults();

        // Paginator - route generator
        $me = $this;
        $routeGenerator = function($page) use ($me, $request)
        {
            $requestParams = $request->query->all();
            $requestParams['pcg_page'] = $page;
            return $me->generateUrl('visita', $requestParams);
        };

        // Paginator - view
        $view = new TwitterBootstrap3View();
        $pagerHtml = $view->render($pagerfanta, $routeGenerator, array(
            'proximity' => 3,
            'prev_message' => 'previous',
            'next_message' => 'next',
        ));

        return array($entities, $pagerHtml);
    }
    
    
    
    /*
     * Calculates the total of records string
     */
    protected function getTotalOfRecordsString($queryBuilder, $request) {
        $totalOfRecords = $queryBuilder->select('COUNT(e.id)')->getQuery()->getSingleScalarResult();
        $show = $request->get('pcg_show', 10);
        $page = $request->get('pcg_page', 1);

        $startRecord = ($show * ($page - 1)) + 1;
        $endRecord = $show * $page;

        if ($endRecord > $totalOfRecords) {
            $endRecord = $totalOfRecords;
        }
        return "Showing $startRecord - $endRecord of $totalOfRecords Records.";
    }
    
    

    /**
     * Displays a form to create a new Visita entity.
     *
     * @Route("/new/{vehiculoid}", name="visita_new")
     * @ParamConverter(class="AppBundle:Vehiculo", name="vehiculo", options={"id" = "vehiculoid"})
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, Vehiculo $vehiculo)
    {
        $visitum = new Visita();
        $visitum->setVehiculo($vehiculo);
        $form   = $this->createForm('AppBundle\Form\VisitaType', $visitum);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($visitum);
            foreach ($visitum->getServiciosRealizados() as $servicioRealizado ) {
                $recordatorio = new Recordatorio();
                $recordatorio->setVehiculo($vehiculo);
                $recordatorio->setServicio($servicioRealizado);
                $triggerAt = $this->get('recordatoriosManager')->getTriggerAt(
                    $visitum->getFecha(),
                    $vehiculo,
                    $servicioRealizado);
                $recordatorio->setTriggerAt($triggerAt);

                $em->persist($recordatorio);
            }
            $em->flush();
            
            $this->get('session')->getFlashBag()->add('success', "Service guardado." );
            
            return $this->redirectToRoute('cliente_edit', [
                'id' => $vehiculo->getPropietario()->getId(),
            ]);
        }
        return $this->render('visita/new.html.twig', array(
            'visitum' => $visitum,
            'form'   => $form->createView(),
        ));
    }
    

    /**
     * Finds and displays a Visita entity.
     *
     * @Route("/{id}", name="visita_show")
     * @Method("GET")
     */
    public function showAction(Visita $visitum)
    {
        $deleteForm = $this->createDeleteForm($visitum);
        return $this->render('visita/show.html.twig', array(
            'visitum' => $visitum,
            'delete_form' => $deleteForm->createView(),
        ));
    }
    
    

    /**
     * Displays a form to edit an existing Visita entity.
     *
     * @Route("/{id}/edit", name="visita_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Visita $visitum)
    {
        $deleteForm = $this->createDeleteForm($visitum);
        $editForm = $this->createForm('AppBundle\Form\VisitaType', $visitum);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($visitum);
            $em->flush();
            
            $this->get('session')->getFlashBag()->add('success', 'Edited Successfully!');
            return $this->redirectToRoute('visita_edit', array('id' => $visitum->getId()));
        }
        return $this->render('visita/edit.html.twig', array(
            'visitum' => $visitum,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    
    

    /**
     * Deletes a Visita entity.
     *
     * @Route("/{id}", name="visita_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Visita $visitum)
    {
    
        $form = $this->createDeleteForm($visitum);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($visitum);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'The Visita was deleted successfully');
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the Visita');
        }
        
        return $this->redirectToRoute('visita');
    }
    
    /**
     * Creates a form to delete a Visita entity.
     *
     * @param Visita $visitum The Visita entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Visita $visitum)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('visita_delete', array('id' => $visitum->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
    
    /**
     * Delete Visita by id
     *
     * @Route("/delete/{id}", name="visita_by_id_delete")
     * @Method("GET")
     */
    public function deleteByIdAction(Visita $visitum)
    {
        $em = $this->getDoctrine()->getManager();
        
        try {
            $em->remove($visitum);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'The Visita was deleted successfully');
        } catch (Exception $ex) {
            $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the Visita');
        }

        return $this->redirect($this->generateUrl('visita'));
    }

    /**
    * Bulk Action
    * @Route("/bulk-action/", name="visita_bulk_action")
    * @Method("POST")
    */
    public function bulkAction(Request $request)
    {
        $ids = $request->get("ids", array());
        $action = $request->get("bulk_action", "delete");

        if ($action == "delete") {
            try {
                $em = $this->getDoctrine()->getManager();
                $repository = $em->getRepository('AppBundle:Visita');

                foreach ($ids as $id) {
                    $visitum = $repository->find($id);
                    $em->remove($visitum);
                    $em->flush();
                }

                $this->get('session')->getFlashBag()->add('success', 'visitas was deleted successfully!');

            } catch (Exception $ex) {
                $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the visitas ');
            }
        }

        return $this->redirect($this->generateUrl('visita'));
    }
}