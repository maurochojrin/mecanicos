<?php

namespace AppBundle\Controller;

use GuzzleHttp\Message\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        if ( $this->getUser()->hasRole('ROLE_ADMIN') ) {
            return $this->redirectToRoute('admin');
        }

        return $this->redirectToRoute('cliente');
    }

    /**
     * @param Request $request
     * @Route("/showPicture/{fileName}", name="showPicture")
     */
    public function showPictureAction(Request $request)
    {
        $pictureFileName = $this->getParameter('picture_path') . '/' . $request->get('fileName');
        $response = new \Symfony\Component\HttpFoundation\Response();

        if ( is_readable($pictureFileName) ) {
            $response->setContent( file_get_contents( $pictureFileName ) );
            $response->headers->set('Content-Type',mime_content_type($pictureFileName));
        } else {
            $response->setStatusCode( 404 );
        }

        return $response;
    }
}
