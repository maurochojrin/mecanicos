<?php

namespace AppBundle\Controller;

use AppBundle\AppBundle;
use AppBundle\Entity\Taller;
use Doctrine\Common\Collections\Criteria;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\View\TwitterBootstrap3View;

use AppBundle\Entity\Cliente;

/**
 * Cliente controller.
 *
 * @Route("/cliente")
 */
class ClienteController extends Controller
{
    /**
     * Lists all Cliente entities.
     *
     * @Route("/", name="cliente")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em
            ->getRepository('AppBundle:Cliente')
            ->createQueryBuilder('e');

        $taller = $this->getUser()->getTaller();
        $queryBuilder->addCriteria(
            Criteria::create()->where(
                Criteria::expr()->eq("taller", $taller )
            )
        );
        
        list($filterForm, $queryBuilder) = $this->filter($queryBuilder, $request);
        list($clientes, $pagerHtml) = $this->paginator($queryBuilder, $request);
        
        return $this->render('cliente/index.html.twig', array(
            'clientes' => $clientes,
            'pagerHtml' => $pagerHtml,
            'filterForm' => $filterForm->createView(),

        ));
    }

    /**
    * Create filter form and process filter request.
    *
    */
    protected function filter($queryBuilder, Request $request)
    {
        $session = $request->getSession();
        $filterForm = $this->createForm('AppBundle\Form\ClienteFilterType');

        // Reset filter
        if ($request->get('filter_action') == 'reset') {
            $session->remove('ClienteControllerFilter');
        }

        // Filter action
        if ($request->get('filter_action') == 'filter') {
            // Bind values from the request
            $filterForm->handleRequest($request);

            if ($filterForm->isValid()) {
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set('ClienteControllerFilter', $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has('ClienteControllerFilter')) {
                $filterData = $session->get('ClienteControllerFilter');
                
                foreach ($filterData as $key => $filter) { //fix for entityFilterType that is loaded from session
                    if (is_object($filter)) {
                        $filterData[$key] = $queryBuilder->getEntityManager()->merge($filter);
                    }
                }
                
                $filterForm = $this->createForm('AppBundle\Form\ClienteFilterType', $filterData);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        return array($filterForm, $queryBuilder);
    }


    /**
    * Get results from paginator and get paginator view.
    *
    */
    protected function paginator($queryBuilder, Request $request)
    {
        //sorting
        $sortCol = $queryBuilder->getRootAlias().'.'.$request->get('pcg_sort_col', 'id');
        $queryBuilder->orderBy($sortCol, $request->get('pcg_sort_order', 'desc'));
        // Paginator
        $adapter = new DoctrineORMAdapter($queryBuilder);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage($request->get('pcg_show' , 10));

        try {
            $pagerfanta->setCurrentPage($request->get('pcg_page', 1));
        } catch (\Pagerfanta\Exception\OutOfRangeCurrentPageException $ex) {
            $pagerfanta->setCurrentPage(1);
        }
        
        $entities = $pagerfanta->getCurrentPageResults();

        // Paginator - route generator
        $me = $this;
        $routeGenerator = function($page) use ($me, $request)
        {
            $requestParams = $request->query->all();
            $requestParams['pcg_page'] = $page;
            return $me->generateUrl('cliente', $requestParams);
        };

        // Paginator - view
        $view = new TwitterBootstrap3View();
        $pagerHtml = $view->render($pagerfanta, $routeGenerator, array(
            'proximity' => 3,
            'prev_message' => 'anterior',
            'next_message' => 'siguiente',
        ));

        return array($entities, $pagerHtml);
    }
    
    

    /**
     * Displays a form to create a new Cliente entity.
     *
     * @Route("/new", name="cliente_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $cliente = new Cliente();
        $form = $this->createForm('AppBundle\Form\ClienteType', $cliente);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $cliente->setTaller( $this->getUser()->getTaller() );
            $em->persist($cliente);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', "Cliente registrado correctamente." );

            if ( $request->get('submit') == 'save' ) {
                return $this->redirectToRoute('cliente_edit', [ 'id' => $cliente->getId() ] );
            } else {
                return $this->redirectToRoute('cliente_new');
            }
        }

        return $this->render('cliente/new.html.twig', array(
            'cliente' => $cliente,
            'form'   => $form->createView(),
        ));
    }
    

    /**
     * Displays a form to edit an existing Cliente entity.
     *
     * @Route("/{id}/edit", name="cliente_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Cliente $cliente)
    {
        if ( $cliente->getTaller()->getId() != $this->getUser()->getTaller()->getId() ) {
            // Someone's been naughty...

            throw new AccessDeniedException();
        }
        // @todo: Matar los vehiculos que quedaron huerfanos
        $deleteForm = $this->createDeleteForm($cliente);
        $editForm = $this->createForm('AppBundle\Form\ClienteType', $cliente);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($cliente);
            $em->flush();
            
            $this->get('session')->getFlashBag()->add('success', 'Los datos se modificaron correctamente');
            return $this->redirectToRoute('cliente_edit', array('id' => $cliente->getId()));
        }
        return $this->render('cliente/edit.html.twig', array(
            'cliente' => $cliente,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    
    

    /**
     * Deletes a Cliente entity.
     *
     * @Route("/{id}", name="cliente_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Cliente $cliente)
    {
    
        $form = $this->createDeleteForm($cliente);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($cliente);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'Se eliminaron los datos del cliente');
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the Cliente');
        }
        
        return $this->redirectToRoute('cliente');
    }
    
    /**
     * Creates a form to delete a Cliente entity.
     *
     * @param Cliente $cliente The Cliente entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Cliente $cliente)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cliente_delete', array('id' => $cliente->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
    
    /**
     * Delete Cliente by id
     *
     * @Route("/delete/{id}", name="cliente_by_id_delete")
     * @Method("GET")
     */
    public function deleteByIdAction(Cliente $cliente)
    {
        $em = $this->getDoctrine()->getManager();
        
        try {
            $em->remove($cliente);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'The Cliente was deleted successfully');
        } catch (Exception $ex) {
            $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the Cliente');
        }

        return $this->redirect($this->generateUrl('cliente'));
    }
    

    /**
    * Bulk Action
    * @Route("/bulk-action/", name="cliente_bulk_action")
    * @Method("POST")
    */
    public function bulkAction(Request $request)
    {
        $ids = $request->get("ids", array());
        $action = $request->get("bulk_action", "sendMail");

        if ($action == "delete") {
            try {
                $em = $this->getDoctrine()->getManager();
                $repository = $em->getRepository('AppBundle:Cliente');

                foreach ($ids as $id) {
                    $cliente = $repository->find($id);
                    $em->remove($cliente);
                    $em->flush();
                }

                $this->get('session')->getFlashBag()->add('success', 'clientes was deleted successfully!');

            } catch (Exception $ex) {
                $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the clientes ');
            }
        } elseif ( $action == "sendMail" ) {
            $this->get('session')->set( 'emailTo', $request->get('ids') );

            return $this->redirect($this->generateUrl('cliente_enviar_correo'));
        }

        return $this->redirect($this->generateUrl('cliente'));
    }

    /**
     * @param Request $request
     * @Route("/enviarCorreo", name="cliente_enviar_correo")
     * @Method({"GET", "POST"})
     */
    public function enviarCorreoAction(Request $request)
    {
        $form = $this->createForm('AppBundle\Form\CorreoType');
        $form->handleRequest( $request );

        $preview = $subject = '';

        if ( $form->isSubmitted() && $form->isValid() ) {
            $responsable = $this->getUser();
            $taller = $responsable->getTaller();

            if ( $request->get('submit') == 'preview' ) {
                $subject = $form['subject']->getData();
                $cliente = new Cliente();
                $cliente->setNombre('[Cliente]');
                $preview = $this->renderView('::mail_base.html.twig',
                    [
                        'body' => $form['message']->getData(),
                        'taller' => $taller,
                        'responsable' => $responsable,
                        'cliente' => $cliente,
                    ]);
            } elseif ( $request->get('submit') == 'send' ) {
                $qb = $clientes = $this->getDoctrine()->getEntityManager()->createQueryBuilder();

                $qb->select('p')
                   ->from('AppBundle:Cliente', 'p');

                if ( $this->get('session')->has('emailTo') ) {
                    $qb->where('p.id IN (:ids)')
                        ->setParameter('ids', $this->get('session')->get('emailTo') );
                } else {
                    $qb->where('p.taller = :taller_id')
                        ->setParameter( 'taller_id', $taller->getId() );
                }

                $q = $qb->getQuery();

                $parameters = [
                    'body' => $form['message']->getData(),
                    'taller' => $taller,
                    'responsable' => $responsable,
                ];

                $swift_message = \Swift_Message::newInstance()
                    ->setSubject( $form['subject']->getData() )
                    ->setFrom( $responsable->getEmail(), $responsable->getNombre() )
                ;

                $mailer = $this->get('mailer');
                $enviados = 0;
                foreach ( $q->getResult() as $cliente ) {
                    $parameters['cliente'] = $cliente;
                    $swift_message
                        ->setTo( $cliente->getEmail(), $cliente->getNombre() )
                        ->addPart(
                            $this->renderView(
                                '::mail_base.html.twig',
                                $parameters
                            ), 'text/html' )
                    ;

                    if ( $mailer->send($swift_message) ) {
                        $enviados++;
                    }
                }

                $this->addFlash('success', 'Se enviaron '.$enviados.' correos');

                return $this->redirectToRoute('cliente_enviar_correo');
            }
        }

        return $this->render( 'cliente/enviar_correo.html.twig',
            [
                'form' => $form->createView(),
                'preview' => $preview,
                'subject' => $subject,
            ]
        );
    }
}
