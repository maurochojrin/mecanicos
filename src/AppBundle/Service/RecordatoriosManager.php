<?php
/**
 * Created by PhpStorm.
 * User: mauro
 * Date: 3/15/17
 * Time: 11:33 AM
 */

namespace AppBundle\Service;


use AppBundle\Entity\Servicio;
use AppBundle\Entity\Vehiculo;

class RecordatoriosManager
{
    public function getTriggerAt( \DateTime $ultimaRealizacion, Vehiculo $vehiculo, Servicio $servicio )
    {
        $aux = clone $ultimaRealizacion;
        $multiplo = $servicio->getCadaKms() / $vehiculo->getKmsPorAnio();

        $dias = min( round(365 * $multiplo), $servicio->getMaximoMeses() * 30 );

        return $aux->add( new \DateInterval('P'.$dias.'D') );
    }
}