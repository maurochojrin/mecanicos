<?php
namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Builder implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');
        $sc = $this->container->get('security.authorization_checker');
        if ( $sc->isGranted('ROLE_USER') ) {
            $menu->addChild('Clientes',
                [
                    'route' => 'cliente',
                ]);
            $user = $this->container->get('security.token_storage')->getToken()->getUser();

            $userNameMenu = $menu->addChild($user->getUserName());
            $userNameMenu->addChild('Salir',
                [
                    'route' => 'fos_user_security_logout',
                ]);
        }

        return $menu;
    }
}
