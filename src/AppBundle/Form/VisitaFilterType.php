<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;


class VisitaFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', Filters\NumberFilterType::class)
            ->add('fecha', Filters\DateTimeFilterType::class)
            ->add('observaciones', Filters\TextFilterType::class)
        
            ->add('vehiculo', Filters\EntityFilterType::class, array(
                    'class' => 'AppBundle\Entity\Vehiculo',
                    'choice_label' => 'patente',
            )) 
            ->add('serviciosRealizados', Filters\EntityFilterType::class, array(
                    'class' => 'AppBundle\Entity\Servicio',
                    'choice_label' => 'nombre',
            )) 
        ;
        $builder->setMethod("GET");


    }

    public function getBlockPrefix()
    {
        return null;
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'allow_extra_fields' => true,
            'csrf_protection' => false,
            'validation_groups' => array('filtering') // avoid NotBlank() constraint-related message
        ));
    }
}
