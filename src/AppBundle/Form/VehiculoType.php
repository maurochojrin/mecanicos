<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VehiculoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', HiddenType::class )
            ->add('patente')
            ->add('kmsPorAnio', ChoiceType::class, [
                'label' => 'Kms. recorridos por año',
                'choices' => [
                    'Hasta 5.000' => '5000',
                    'Hasta 10.000' => '10000',
                    'Hasta 15.000' => '15000',
                    'Hasta 25.000' => '25000',
                    'Hasta 40.000' => '40000',
                    'Mas de 40.000' => '80000',
                ],
                'data' => '10000',
            ])
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Vehiculo'
        ));
    }
}
