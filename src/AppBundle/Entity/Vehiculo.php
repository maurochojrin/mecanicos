<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Vehiculo
 *
 * @ORM\Table(name="vehiculo")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\VehiculoRepository")
 */
class Vehiculo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="patente", type="string", length=255, unique=true)
     */
    private $patente;

    /**
     * @var int
     *
     * @ORM\Column(name="kmsPorAnio", type="integer")
     */
    private $kmsPorAnio;

    /**
     * @var Cliente
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Cliente", inversedBy="vehiculos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $propietario;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Recordatorio", mappedBy="vehiculo", cascade={"remove"})
     */
    private $recordatorios;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Visita", mappedBy="vehiculo", cascade={"remove"})
     */
    private $visitas;

    /**
     * @return ArrayCollection
     */
    public function getVisitas()
    {
        return $this->visitas;
    }

    /**
     * @param ArrayCollection $visitas
     * @return Vehiculo
     */
    public function setVisitas(ArrayCollection $visitas): Vehiculo
    {
        $this->visitas = $visitas;
        return $this;
    }

    /**
     * @return Cliente
     */
    public function getPropietario()
    {
        return $this->propietario;
    }

    /**
     * @param Cliente $propietario
     * @return Vehiculo
     */
    public function setPropietario(Cliente $propietario)
    {
        $this->propietario = $propietario;
        return $this;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function setId( $id )
    {
        $this->id = $id;
    }

    /**
     * Set patente
     *
     * @param string $patente
     *
     * @return Vehiculo
     */
    public function setPatente($patente)
    {
        $this->patente = $patente;

        return $this;
    }

    /**
     * Get patente
     *
     * @return string
     */
    public function getPatente()
    {
        return $this->patente;
    }

    /**
     * Set kmsPorAnio
     *
     * @param integer $kmsPorAnio
     *
     * @return Vehiculo
     */
    public function setKmsPorAnio($kmsPorAnio)
    {
        $this->kmsPorAnio = $kmsPorAnio;

        return $this;
    }

    /**
     * Get kmsPorAnio
     *
     * @return int
     */
    public function getKmsPorAnio()
    {
        return $this->kmsPorAnio;
    }

    /**
     * @return ArrayCollection
     */
    public function getRecordatorios()
    {
        return $this->recordatorios;
    }

    /**
     * @param ArrayCollection $recordatorios
     * @return Vehiculo
     */
    public function setRecordatorios($recordatorios)
    {
        $this->recordatorios = $recordatorios;
        return $this;
    }

    public function __construct()
    {
        $this->setVisitas( new ArrayCollection() );
        $this->setRecordatorios( new ArrayCollection() );
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getPatente();
    }
}

