<?php
// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string",name="foto",nullable=true)
     */
    protected $foto;

    /**
     * @var Taller
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Taller",mappedBy="responsable")
     */
    protected $taller;

    /**
     * @var string
     * @ORM\Column(name="nombre",nullable=true,type="string")
     */
    protected $nombre;

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return User
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return User
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return string
     */
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * @param string $foto
     * @return User
     */
    public function setFoto($foto)
    {
        $this->foto = $foto;
        return $this;
    }

    /**
     * @return Taller
     */
    public function getTaller()
    {
        return $this->taller;
    }

    /**
     * @param Taller $taller
     * @return User
     */
    public function setTaller(Taller $taller = null)
    {
        $this->taller = $taller;

        return $this;
    }
}
