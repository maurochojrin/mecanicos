<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Servicio
 *
 * @ORM\Table(name="servicio")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ServicioRepository")
 */
class Servicio
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255, unique=true)
     */
    private $nombre;

    /**
     * @var int
     *
     * @ORM\Column(name="cadaKms", type="integer")
     */
    private $cadaKms;

    /**
     * @var int
     *
     * @ORM\Column(name="maximo_meses", type="integer")
     */
    private $maximoMeses;

    /**
     * @var string
     *
     * @ORM\Column(name="consecuenciasNoHacerlo", type="text")
     */
    private $consecuenciasNoHacerlo;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Servicio
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set cadaKms
     *
     * @param integer $cadaKms
     *
     * @return Servicio
     */
    public function setCadaKms($cadaKms)
    {
        $this->cadaKms = $cadaKms;

        return $this;
    }

    /**
     * Get cadaKms
     *
     * @return int
     */
    public function getCadaKms()
    {
        return $this->cadaKms;
    }

    /**
     * Set maximoMeses
     *
     * @param integer $maximoMeses
     *
     * @return Servicio
     */
    public function setMaximoMeses($maximoMeses)
    {
        $this->maximoMeses = $maximoMeses;

        return $this;
    }

    /**
     * Get maximoMeses
     *
     * @return int
     */
    public function getMaximoMeses()
    {
        return $this->maximoMeses;
    }

    /**
     * Set consecuenciasNoHacerlo
     *
     * @param string $consecuenciasNoHacerlo
     *
     * @return Servicio
     */
    public function setConsecuenciasNoHacerlo($consecuenciasNoHacerlo)
    {
        $this->consecuenciasNoHacerlo = $consecuenciasNoHacerlo;

        return $this;
    }

    /**
     * Get consecuenciasNoHacerlo
     *
     * @return string
     */
    public function getConsecuenciasNoHacerlo()
    {
        return $this->consecuenciasNoHacerlo;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getNombre();
    }
}

