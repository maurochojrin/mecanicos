<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Recordatorio
 *
 * @ORM\Table(name="recordatorio")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RecordatorioRepository")
 */
class Recordatorio
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="trigger_at", type="date")
     */
    private $triggerAt;

    /**
     * @var Vehiculo
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Vehiculo",inversedBy="recordatorios")
     * @ORM\JoinColumn(nullable=false)
     */
    private $vehiculo;

    /**
     * @var Servicio
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Servicio")
     * @ORM\JoinColumn(nullable=false)
     */
    private $servicio;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set triggerAt
     *
     * @param \DateTime $triggerAt
     *
     * @return Recordatorio
     */
    public function setTriggerAt(\DateTime $triggerAt)
    {
        $this->triggerAt = $triggerAt;

        return $this;
    }

    /**
     * @return Vehiculo
     */
    public function getVehiculo()
    {
        return $this->vehiculo;
    }

    /**
     * @param Vehiculo $vehiculo
     * @return Recordatorio
     */
    public function setVehiculo(Vehiculo $vehiculo)
    {
        $this->vehiculo = $vehiculo;
        return $this;
    }

    /**
     * @return Servicio
     */
    public function getServicio()
    {
        return $this->servicio;
    }

    /**
     * @param Servicio $servicio
     * @return Recordatorio
     */
    public function setServicio(Servicio $servicio)
    {
        $this->servicio = $servicio;
        return $this;
    }

    /**
     * Get triggerAt
     *
     * @return \DateTime
     */
    public function getTriggerAt()
    {
        return $this->triggerAt;
    }
}

