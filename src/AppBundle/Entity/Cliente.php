<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Cliente
 *
 * @ORM\Table(name="cliente")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ClienteRepository")
 */
class Cliente
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=255)
     */
    private $telefono;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     */
    private $email;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Vehiculo", mappedBy="propietario", cascade={"persist", "remove"})
     */
    private $vehiculos;

    /**
     * @return Taller
     */
    public function getTaller()
    {
        return $this->taller;
    }

    /**
     * @param Taller $taller
     * @return Cliente
     */
    public function setTaller(Taller $taller)
    {
        $this->taller = $taller;

        return $this;
    }

    /**
     * @var Taller
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Taller", inversedBy="vehiculos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $taller;

    /**
     * @return ArrayCollection
     */
    public function getVehiculos()
    {
        return $this->vehiculos;
    }

    /**
     * @param ArrayCollection $vehiculos
     * @return Cliente
     */
    public function setVehiculos($vehiculos)
    {
        $this->vehiculos = $vehiculos;
        return $this;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Cliente
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     *
     * @return Cliente
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set mail
     *
     * @param string $email
     *
     * @return Cliente
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get mail
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getNombre();
    }

    /**
     * @param Vehiculo $vehiculo
     */
    public function addVehiculo(Vehiculo $vehiculo)
    {
        $vehiculo->setPropietario($this);
        $this->vehiculos->add($vehiculo);

        return $this;
    }

    /**
     * @param Vehiculo $vehiculo
     */
    public function removeVehiculo(Vehiculo $vehiculo)
    {
        $vehiculo->setPropietario(null);
        $this->vehiculos->remove( $vehiculo->getId() );

        return $this;
    }

    public function __construct()
    {
        $this->vehiculos = new ArrayCollection();
    }
}

