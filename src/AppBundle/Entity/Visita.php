<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use \DateTime as DateTime;

/**
 * Visita
 *
 * @ORM\Table(name="visita")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\VisitaRepository")
 */
class Visita
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="date")
     */
    private $fecha;

    /**
     * @var string
     *
     * @ORM\Column(name="observaciones", type="text", nullable=true)
     */
    private $observaciones;

    /**
     * @var Vehiculo
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Vehiculo", inversedBy="visitas")
     */
    private $vehiculo;

    /**
     * @return Vehiculo
     */
    public function getVehiculo(): Vehiculo
    {
        return $this->vehiculo;
    }

    /**
     * @param Vehiculo $vehiculo
     * @return Visita
     */
    public function setVehiculo(Vehiculo $vehiculo): Visita
    {
        $this->vehiculo = $vehiculo;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getServiciosRealizados()
    {
        return $this->serviciosRealizados;
    }

    /**
     * @param ArrayCollection $serviciosRealizados
     * @return Visita
     */
    public function setServiciosRealizados(ArrayCollection $serviciosRealizados): Visita
    {
        $this->serviciosRealizados = $serviciosRealizados;
        return $this;
    }

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Servicio", inversedBy="visitas", cascade={"remove"})
     */
    private $serviciosRealizados;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return Visita
     */
    public function setFecha(DateTime $fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha(): DateTime
    {
        return $this->fecha;
    }

    /**
     * Set observaciones
     *
     * @param string $observaciones
     *
     * @return Visita
     */
    public function setObservaciones($observaciones)
    {
        $this->observaciones = $observaciones;

        return $this;
    }

    /**
     * Get observaciones
     *
     * @return string
     */
    public function getObservaciones()
    {
        return $this->observaciones;
    }

    /**
     * Visita constructor.
     */
    public function __construct()
    {
        $this->setFecha( new DateTime() );
        $this->setServiciosRealizados( new ArrayCollection() );
    }

    /**
     * @param Servicio $servicio
     * @return Visita
     */
    public function addServicio(Servicio $servicio): Visita
    {
        $this->serviciosRealizados[] = $servicio;

        return $this;
    }

    /**
     * @param Servicio $servicio
     * @return Visita
     */
    public function removeServicio(Servicio $servicio): Visita
    {
        foreach ( $this->serviciosRealizados as $k => $servicioRealizado ) {
            if ( $servicio->getId() == $servicioRealizado->getId() ) {
                unset( $this->serviciosRealizados[$k] );

                break;
            }
        }
        return $this;
    }
}

