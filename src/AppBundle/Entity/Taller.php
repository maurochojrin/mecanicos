<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Taller
 *
 * @ORM\Table(name="taller")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TallerRepository")
 */
class Taller
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255, unique=true)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="string", length=255)
     */
    private $direccion;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=255)
     */
    private $telefono;

    /**
     * @var string
     *
     * @ORM\Column(name="logo", type="string", length=255)
     */
    private $logo;

    /**
     * @return ArrayCollection
     */
    public function getClientes()
    {
        return $this->clientes;
    }

    /**
     * @param ArrayCollection $clientes
     * @return Taller
     */
    public function setClientes(ArrayCollection $clientes): Taller
    {
        $this->clientes = $clientes;
        return $this;
    }

    /**
     * @var User
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\User", inversedBy="taller")
     */
    private $responsable;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Cliente", mappedBy="taller", cascade={"remove"})
     */
    private $clientes;

    /**
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * @param string $website
     * @return Taller
     */
    public function setWebsite($website)
    {
        $this->website = $website;
        return $this;
    }

    /**
     * @var string
     * @ORM\Column(name="link_mapa",nullable=true,type="string")
     */
    private $linkMapa;

    /**
     * @var string
     * @ORM\Column(name="website",nullable=true,type="string")
     */
    private $website;

    /**
     * @return string
     */
    public function getLinkMapa()
    {
        return $this->linkMapa;
    }

    /**
     * @param string $linkMapa
     * @return Taller
     */
    public function setLinkMapa($linkMapa)
    {
        $this->linkMapa = $linkMapa;
        return $this;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Taller
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     *
     * @return Taller
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     *
     * @return Taller
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set logo
     *
     * @param string $logo
     *
     * @return Taller
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    public function __construct()
    {
        $this->setServiciosOfrecidos(new ArrayCollection());
        $this->setVehiculos( new ArrayCollection() );
    }

    /**
     * @return User
     */
    public function getResponsable()
    {
        return $this->responsable;
    }

    /**
     * @param User $responsable
     * @return Taller
     */
    public function setResponsable($responsable)
    {
        $this->responsable = $responsable;
        return $this;
    }

    /**
     * @return string
     */
    function __toString()
    {
        return $this->getNombre();
    }
}

