<?php
/**
 * Created by PhpStorm.
 * User: mauro
 * Date: 3/15/17
 * Time: 11:40 AM
 */

namespace AppBundle\Command;

use AppBundle\Entity\Recordatorio;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;

class EnviarRecordatoriosCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('mecanicos:enviar-recordatorios')
            // the short description shown while running "php bin/console list"
            ->setDescription('Envia un recordatorio a todos los clientes que deben realizar un mantenimiento en su vehiculo.')
            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp("Envia un recordatorio a todos los clientes que deben realizar un mantenimiento en su vehiculo.");
        $this
            // configure an argument
            ->addOption('go')
            ->addOption('limit', 'l', InputOption::VALUE_REQUIRED, 'Maximo de monotributistas procesados');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Comienza el envio de recordatorios'.( $input->getOption('go') ? ' (For real)' : '' ));
        $output->writeln('=================================='.( $input->getOption('go') ? '===========' : '' ));

        $verbose = $input->getOption('verbose');
        if ($verbose) {
            $output->writeln('Buscando recordatorios para enviar');
        }

        $em = $this->getContainer()->get('Doctrine');
        $recordatorios = $em->getRepository('AppBundle:Recordatorio')->findBy(['triggerAt' => new \DateTime()]);

        if ( $verbose ) {
            $output->writeln('Se encontraron '.count($recordatorios).' recordatorios para enviar');
        }

        $enviados = 0;

        $swift_Mailer = $this->getContainer()->get('mailer');

        foreach ($recordatorios as $recordatorio ) {
            $swift_Message = $this->crearMensaje($recordatorio);

            if ( $input->getOption('go') ) {
                if ( $swift_Mailer->send($swift_Message) ) {
                    if ( $verbose ) {
                        $output->writeln('Enviando mail a '.implode( ',', $swift_Message->getTo() ) );
                    }
                    $em->getEntityManager()->remove( $recordatorio );
                    if ( $verbose ) {
                        $output->writeln('Enviado, recoratorio eliminado');
                    }
                    $enviados++;
                } else {
                    $recordatorio->setTriggerAt( new \DateTime('tomorrow') );
                    $output->writeln('No pudo enviarse el recordatorio '.$recordatorio->getId().' se reintentara mañana');
                }
            } else {
                $enviados++;
            }
        }

        $em->getEntityManager()->flush();

        $output->writeln('Proceso finalizado');
        $output->writeln('Se enviaron '.$enviados.' recordatorios');
    }

    /**
     * @param Recordatorio $recordatorio
     * @return \Swift_Message
     */
    private function crearMensaje(Recordatorio $recordatorio)
    {
        $vehiculo = $recordatorio->getVehiculo();
        $cliente = $vehiculo->getPropietario();
        $taller = $cliente->getTaller();
        $responsable = $taller->getResponsable();
        $servicio = $recordatorio->getServicio();

        return \Swift_Message::newInstance()
            ->setSubject('Se recomienda realizar un servicio de mantenimiento de su vehiculo '.$vehiculo->getPatente() )
            ->setFrom( $responsable->getEmail(), $responsable->getNombre() )
            ->setTo($cliente->getEmail(), $cliente->getNombre())
            ->addPart(
                $this
                    ->getContainer()
                    ->get('templating')
                    ->render( 'recordatorio.html.twig', [
                        'vehiculo' => $vehiculo,
                        'cliente' => $cliente,
                        'taller' => $taller,
                        'responsable' => $responsable,
                        'servicio' => $servicio,
                    ] ),
                'text/html'
            )
        ;
    }
}